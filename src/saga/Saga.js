import { put, takeLatest, fork, all } from 'redux-saga/effects'

const apiKey = 'bd4dba0299c57b3c8cacbfedacbb63f2';
const token = '01f89a747228ef2fc39c0e36e9c9a358cb47726ea2541561752359b9c0de787f';
const boardId = '5dd39433d531733fa786a462';
const url = `https://api.trello.com/1/boards/${boardId}/lists?cards=all&key=${apiKey}&token=${token}`;


function* getLists(){
    console.log('get lists')
    const json = yield fetch(url).then(res => res.json())
    yield put({type: 'fetchedData', json})
}

function* actionWatcher1(){
    yield takeLatest('getAll', getLists)
    // yield getLists()
}

function* getCardDetails(card){
    console.log('Get Card Details')
    const cardId = card.data
    // console.log(card.data)
    const cardUrl = `https://api.trello.com/1/cards/${cardId}?key=${apiKey}&token=${token}`;
    const json = yield fetch(cardUrl).then(res => res.json()).then(data => data)
    yield put({type: 'cardData', json})
}

function* actionWatcher2(){
    yield takeLatest('getCards', getCardDetails)
    // yield getLists()
}

function* getChecklists(card){
    console.log('Get Checklists')
    const cardId = card.data
    // console.log(card.data)
    const checkListUrl = `https://api.trello.com/1/cards/${cardId}/checklists?checkItems=all&checkItem_fields=all&filter=all&fields=all&key=${apiKey}&token=${token}`;
    const json = yield fetch(checkListUrl).then(res => res.json()).then(data => data)
    yield put({type: 'checklists', json})
}

function* actionWatcher3(){
    yield takeLatest('getCheck', getChecklists)
}

function* addACard(data){
    console.log('Add Card')
    const listId = data.listId
    const input = data.input
    // console.log(listId + input)
    // console.log(card.data)
    const addCardUrl = `https://api.trello.com/1/cards?idList=${listId}&name=${input}&key=${apiKey}&token=${token}`;
    const json = yield fetch(addCardUrl, {method: 'POST'})
    json['cards'] = [];
    yield put({type: 'addList', json})
}

function* actionWatcher4(){
    yield takeLatest('Card', addACard)
}

function* addAList(data){
    console.log('Add List')
    const input = data.input
    console.log(input)
    const addListUrl = `https://api.trello.com/1/lists?name=${input}&idBoard=${boardId}&pos=bottom&key=${apiKey}&token=${token}`
    const json = yield fetch(addListUrl, {method: 'POST'})
    json['cards'] = [];
    json['name'] = input
    yield put({type: 'addList', json})
}

function* actionWatcher5(){
    yield takeLatest('list', addAList)
}

function* deleteCard(data){
    const cardId = yield data.cardId
    console.log(cardId)
    const deleteCardUrl = `https://api.trello.com/1/cards/${cardId}?key=${apiKey}&token=${token}`;
    const json = yield fetch(deleteCardUrl, {method: 'DELETE'})
    json['cards'] = [];
    yield put({type: 'addList', json})
}

function* actionWatcher6(){
    yield takeLatest('delCard', deleteCard)
}

function* editCard(data){
    const cardId = data.cardId
    const input = data.input
    // console.log(cardId)
    const Url = `https://api.trello.com/1/cards/${cardId}?name=${input}&key=${apiKey}&token=${token}`
    const json = yield fetch(Url, {method: 'PUT'})
    json['cards'] = [];
    yield put({type: 'addList', json})
}

function* actionWatcher7(){
    yield takeLatest('edCard', editCard)
}

function* addChecklistItem(data){
    const checklistId = data.checklistId
    const input = data.input
    const url = `https://api.trello.com/1/checklists/${checklistId}/checkItems?name=${input}&pos=bottom&checked=false&key=${apiKey}&token=${token}`;
    // console.log(checklistId)
    const json = yield fetch(url, {method: 'POST'})
    json['checkItems'] = [];
    yield put({type: 'addChecklist', json})
}

function* actionWatcher8(){
    yield takeLatest('addItem', addChecklistItem)
}

function* addChecklist(data){
    const cardId = data.cardId
    const input = data.input
    const url = `https://api.trello.com/1/checklists?idCard=${cardId}&name=${input}&key=${apiKey}&token=${token}`;
    // console.log(checklistId)
    const json = yield fetch(url, {method: 'POST'})
    json['checkItems'] = [];
    yield put({type: 'addChecklist', json})
    // getChecklists(cardId)
}

function* actionWatcher9(){
    yield takeLatest('Checklist', addChecklist)
}

function* delChecklist(data){
    const checklistId = data.checklistId
    const url = `https://api.trello.com/1/checklists/${checklistId}?key=${apiKey}&token=${token}`;
    // console.log(checklistId)
    const json = yield fetch(url, {method: 'DELETE'})
    json['checkItems'] = [];
    yield put({type: 'addChecklist', json})
}

function* actionWatcher10(){
    yield takeLatest('delete', delChecklist)
}

export default function* rootSaga(){
    yield all([
        fork(actionWatcher1),
        fork(actionWatcher2),
        fork(actionWatcher3),
        fork(actionWatcher4),
        fork(actionWatcher5),
        fork(actionWatcher6),
        fork(actionWatcher7),
        fork(actionWatcher8),
        fork(actionWatcher9),
        fork(actionWatcher10),
    ])
}