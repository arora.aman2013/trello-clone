import React, { Component } from 'react';
import './App.css';
import { Provider } from 'react-redux';
import store from './store/index'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'
import Lists from './components/Lists'
import CardDescription from './components/CardDescription'
import EditCard from './components/EditCard'

// import SagaData from './sagaData';

class App extends Component{
  render(){
    return(
      <Provider store = {store} >
      <Router>
        <Route path = '/' component = {Lists} />
        <Route path = '/card/:cardId' render = {(props) => (
              <CardDescription params = {props.match.params} />
            )} /> 
        <Route path = '/edit/:cardId/:cardName' render = {(props) => (
              <EditCard params = {props.match} />
            )} /> 
      </Router>
      {/* <SagaData /> */}
    </Provider>
    )
  }
}

export default App;
