const initialState = {
    listsAndCards: [],
    cardDetails: [],
    checklists: []
}

const data = (state = initialState, action) => {
    // console.log(action)
    switch(action.type) {
        case 'fetchedData':
            return {
                ...state,
                listsAndCards: action.json
            }
        case 'cardData':
            return {
                ...state,
                cardDetails: action.json
            }
        case 'checklists':
            return {
                ...state,
                checklists: action.json
            }
        case 'addList': 
            // console.log(action.json)
            return{
                ...state,
                listsAndCards: state.listsAndCards.concat(action.json)
            }
        case 'addChecklist': 
            // console.log(action.json)
            return{
                ...state,
                checklists: state.checklists.concat(action.json)
            }
        default: return state;
    }
}

export default data