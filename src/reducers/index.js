import { combineReducers } from 'redux'
import data from './trelloReducer'

export default combineReducers({
    lists: data
})