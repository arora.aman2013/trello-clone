import React, { Component } from 'react'
import CheckListItems from './CheckListItems'
import { addCheckItem, deleteChecklist } from '../actions/trelloActions'
import { connect } from 'react-redux'

export class CheckLists extends Component {
    state = {
        formOpen: false,
        text: ''
    }
    renderButton = () => {
        return(
            <button onClick = {this.onClick}>Add Item</button>
        )
    }
    onClick = (e) => {
        e.stopPropagation()
        this.setState({formOpen: true})
    }
    onChange = (e) => {
        this.setState({text: e.target.value})
        // console.log(e.target.value, this.props.checklist.id)
    }
    submit = (e) => {
        e.preventDefault()
        if(this.state.text){
            this.props.addCheckItem(this.props.checklist.id, this.state.text)
            this.setState({formOpen: false, text: ''})
        }
    }
    renderForm = () => {
        return(
            <form onChange = {this.onChange} onSubmit = {this.submit}>
                <input type = 'text' placeholder = 'Enter checklist item' autoFocus onBlur = {() => {this.setState({formOpen: false})}} />
                <input type = 'submit' />
            </form>
        )
    }
    delete = (e) =>{
        e.stopPropagation()
        this.props.deleteChecklist(this.props.checklist.id)
    }
    render() {
        return (
            <div>
                <p style = {{display: 'flex', justifyContent: 'space-between'}}>
                    <h3>{this.props.checklist.name}</h3>
                    <button style = {{border: 'none', backgroundColor: 'transparent', cursor: 'pointer',}} onClick= {this.delete} >X</button>
                </p>
                <div>
                    {this.props.checklist.checkItems.map((checklistItem => (
                        <CheckListItems key = {checklistItem.id} checklistItem = {checklistItem} />
                    )))}
                </div>
                {this.state.formOpen ? this.renderForm() : this.renderButton()}
            </div>
        )
    }
}

export default connect(null, { addCheckItem, deleteChecklist })(CheckLists)
