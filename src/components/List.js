import React, { Component } from 'react'
import Cards from './Cards'
import Card from '@material-ui/core/Card';
import Textarea from 'react-textarea-autosize'
import { connect } from 'react-redux'
import { addCard, getListsAndCards } from '../actions/trelloActions'

class List extends Component {
    state = {
        formOpen: false,
        text: ''
    }
    renderButton = () => {
        return(
            <button style = {buttonStyle} onClick = {this.onClick}>+ Add Another Card</button>
        )
    }
    onClick = () => {
        this.setState({formOpen: true})
    }
    closeForm = () => {
        this.setState({formOpen: false})
    }
    handleChange = (e) => {
        // console.log(e.target.value)
        this.setState({
            text: e.target.value
        })
    }
    onKey = (e) => {
        if(e.key === 'Enter'){
            // console.log(e.target.value)
            this.addCard()
        }
    }
    addCard = () => {
        if(this.state.text !== ''){
            this.props.addCard(this.props.list.id, this.state.text)
            this.setState({text: '', formOpen: false})
        }
    }
    renderForm = () => {
        return(
            <div>
                <Card style = {{overflow: 'visible', minHeight: 60, minWidth: 100, padding: '6px 8px 2px', marginBottom: '2%'}}>
                    <Textarea onKeyPress = {this.onKey} placeholder = 'Enter Card Title' style = {{resize: 'none', width: '100%', border: 'none', overflow: 'hidden', outline: 'none'}} autoFocus onBlur = {this.closeForm} value = {this.state.text} onChange = {this.handleChange} />
                </Card>
                <div style = {{display: 'flex'}} >
                    <button style = {{backgroundColor: '#5aac44', color: 'white', border: 'none'}} onMouseDown = {this.addCard}>Add</button>
                    <button style = {{border: 'none', backgroundColor: 'transparent'}}>X</button>
                </div>
            </div>
        )
    }
    // componentDidUpdate(prevProps){
    //     if(prevProps.list.cards.length !== this.props.list.cards.length){
    //         this.props.getListsAndCards();
    //     }
    // }
    render() {
        return (
            <div style = {listStyle}>
                {this.props.list.name}
                <div style = {cardStyle} >
                    {this.props.list.cards.map((card => (
                            <Cards key = {card.id} card = {card} />
                        )))}
                </div>
                {this.state.formOpen ? this.renderForm() : this.renderButton()}
            </div>
        )
    }
}

const listStyle = {
    minWidth: '200px',
    backgroundColor: '#ebecf0',
    marginRight: '2%',
    padding: '1%',
    height: 'fit-content'
}

const cardStyle = {
    overflowY: 'auto',
    maxHeight: '80vh',
}

const buttonStyle = {
    width: '90%',
    cursor: 'pointer',
    paddingTop: '2%',
    background: 'transparent',
    border: 'none',
    textAlign: 'left'
}

export default connect(null, { addCard, getListsAndCards })(List)
