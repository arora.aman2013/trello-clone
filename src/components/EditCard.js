import React, { Component } from 'react'
import Textarea from 'react-textarea-autosize'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import { editCard } from '../actions/trelloActions'


class EditCard extends Component {
    state = {
        text: ''
    }
    componentDidMount(){
        // console.log(cardId)
        // console.log(this.props.params.params.cardName)
        // this.props.getCard(cardId)
        // this.props.getCheckList(cardId)
        this.setState({text: this.props.params.params.cardName})
    }
    CloseEdit = () => {
        this.props.history.push('/')
    }
    onKey = (e) => {
        if(e.key === 'Enter'){
            this.submit()
        }
    }
    onChange = (e) => {
        this.setState({text:e.target.value})
    }
    submit = async () => {
        const cardId = this.props.params.params.cardId;
        if(this.state.text){
            let x = await this.props.editCard(cardId, this.state.text)
            this.props.history.push('/')
        }
    }
    render() {
        return (
            <div style = {blur}>
                <div style = {modal}>
                    <Textarea onKeyPress = {this.onKey} placeholder = 'Enter List Title' style = {{resize: 'none', width: '100%', border: 'none', overflow: 'hidden', outline: 'none'}} autoFocus value = {this.state.text} onBlur = {this.CloseEdit} onChange = {this.onChange}/>
                </div>
            </div>
        )
    }
}

const blur = {
    backgroundColor: 'rgba(0,0,0,0.4)',
    position: 'fixed',
    zIndex: '2',
    left: '0',
    top: '0',
    height: '100%',
    width: '100%',
    overflowY: 'auto',
}

const modal = {
    // display: 'none',
    backgroundColor: '#ebecf0',
    width: '50%',
    margin: 'auto',
    marginTop: '10vh',
    lineHeight: '2',
    padding: '2%',
}

const mapStateToProps = (state) => {
    return{
        cardDetails: state.lists.cardDetails,
        checklist: state.lists.checklists
    }
}

export default connect(mapStateToProps, { editCard })(withRouter(EditCard))
