import React, { Component } from 'react'
import { connect } from 'react-redux'
// import axios from 'axios'
import List from './List'
import Card from '@material-ui/core/Card';
import Textarea from 'react-textarea-autosize'
import { getListsAndCards, addList } from '../actions/trelloActions'

class Lists extends Component {
    state = {
        formOpen: false,
        text: ''
    }
    renderButton = () => {
        return(
            <button style = {buttonStyle} onClick = {this.onClick}>+ Add Another List</button>
        )
    }
    onClick = () => {
        this.setState({formOpen: true})
    }
    closeForm = () => {
        this.setState({state: '', formOpen: false})
    }
    handleChange = (e) => {
        this.setState({
            text: e.target.value
        })
    }
    onKey = (e) => {
        if(e.key === 'Enter'){
            this.submit()
        }
    }
    submit = () => {
        if(this.state.text !== ''){
            this.props.addList(this.state.text)
            this.setState({formOpen: false})
            // this.props.getListsAndCards();
            // this.componentDidMount()
            // this.setState({formOpen: false})
        }
    }
    renderForm = () => {
        return(
            <div style = {liststyle}>
                <Card style = {{overflow: 'visible', minHeight: 80, minWidth: 100, padding: '6px 8px 2px', marginBottom: '2%'}}>
                    <Textarea onKeyPress = {this.onKey} placeholder = 'Enter List Title' style = {{resize: 'none', width: '100%', border: 'none', overflow: 'hidden', outline: 'none'}} autoFocus onBlur = {this.closeForm} value = {this.state.text} onChange = {this.handleChange} />
                </Card>
                <div style = {{display: 'flex'}} >
                    <button onMouseDown = {this.submit}>Add</button>
                    <button style = {{border: 'none', backgroundColor: 'transparent'}}>X</button>
                </div>
            </div>
        )
    }
    componentDidMount(){
        this.props.getListsAndCards()
        // console.log(this.props.lists)
        // axios(url).then(res => this.setState({lists: res.data}, console.log(res.data))); 
    }
    componentDidUpdate(prevProps){
        // console.log(prevProps)
        // console.log(this.props.lists)
        if(prevProps.lists.length !== this.props.lists.length){
            this.props.getListsAndCards();
        // console.log('Hello')
            // this.componentDidMount()
        }
    }
    render() {
        return (
            <div style = {{ display: 'flex' }}>
                {this.props.lists.map(list =><List key = {list.id} list = {list} />)}
                {this.state.formOpen ? this.renderForm() : this.renderButton()}
            </div>
        )
    }
}

const liststyle = {
    minWidth: '200px',
    backgroundColor: '#ebecf0',
    marginRight: '2%',
    padding: '1%',
    height: 'fit-content',
    overflowX: 'auto'
}

const buttonStyle = {
    minWidth: '200px',
    backgroundColor: 'rgba(0,0,0,0.15)',
    marginRight: '2%',
    padding: '1%',
    height: 'fit-content',
    float: 'top',
    cursor: 'pointer',
    opacity: '0.5',
    border: 'none',
    textAlign: 'left'
}



const mapStateToProps = (state) => {
    return{
        lists: state.lists.listsAndCards
    }
}

export default connect(mapStateToProps, { getListsAndCards, addList })(Lists)
// export default Lists
