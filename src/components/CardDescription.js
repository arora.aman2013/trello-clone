import React, { Component } from 'react'
import Checklists from './CheckLists'
import { withRouter } from 'react-router-dom'
import { getCard, getCheckList, addChecklist } from '../actions/trelloActions'
import { connect } from 'react-redux'

class CardDescription extends Component {
    state = {
        formOpen: false,
        text: ''
    }
    componentDidMount(){
        const cardId = this.props.params.cardId;
        // console.log(cardId)
        this.props.getCard(cardId)
        this.props.getCheckList(cardId)
    }
    componentDidUpdate(prevProps){
        if(prevProps.checklist.length !== this.props.checklist.length){
            this.props.getCheckList(this.props.params.cardId)
        }
    }
    onClick = (e) => {
        e.stopPropagation()
        // console.log(e.target)
        // console.log(document.getElementById('modal'))
        // if(e.target !== document.getElementById('modal')){
        //     this.props.history.push('/')
        // }
    }
    renderButton = () => {
        return(
            <button onClick = {this.onClick}>Add CheckList</button>
        )
    }
    onClick = () => {
        this.setState({formOpen: true})
    }
    close = (e) => {
        this.props.history.push('/')
    }
    onChange = (e) => {
        this.setState({text: e.target.value})
        // console.log(e.target.value)
    }
    submit = (e) => {
        e.preventDefault()
        this.props.addChecklist(this.props.cardDetails.id, this.state.text)
        this.setState({formOpen: false, text: ''})
    }
    renderForm = () => {
        return(
            <form onChange = {this.onChange} onSubmit = {this.submit}>
                <input type = 'text' placeholder = 'Enter checklist' autoFocus onBlur = {() => {this.setState({formOpen: false})}} />
                <input type = 'submit' />
            </form>
        )
    }
    
    render() {
        return (
            <div style = {blur} onClick = {this.onClick}>
                <div style = {modal} id = 'modal'>
                    <div  style = {{display: 'flex', justifyContent: 'space-between'}}>
                        <h3>{this.props.cardDetails.name}</h3>
                        <button style = {{border: 'none', cursor: 'pointer', backgroundColor: 'transparent'}} onClick = {this.close} >X</button>
                    </div>
                    <div>
                        <h3>Description</h3>
                        <p>{this.props.cardDetails.desc}</p>
                        <div>
                            {this.props.checklist.map((checkList => (
                                <Checklists key = {checkList.id} checklist = {checkList} blur = {this.blur} />
                                )))}
                        </div>
                    </div>
                        {this.state.formOpen ? this.renderForm() : this.renderButton()}
                </div>
            </div>
        )
    }
}

const blur = {
    backgroundColor: 'rgba(0,0,0,0.4)',
    position: 'fixed',
    zIndex: '2',
    left: '0',
    top: '0',
    height: '100%',
    width: '100%',
    overflowY: 'auto',
}

const modal = {
    // display: 'none',
    backgroundColor: '#ebecf0',
    width: '50%',
    margin: 'auto',
    marginTop: '10vh',
    lineHeight: '2',
    padding: '2%',
}

const mapStateToProps = (state) => {
    return{
        cardDetails: state.lists.cardDetails,
        checklist: state.lists.checklists
    }
}



export default connect(mapStateToProps, { getCard, getCheckList, addChecklist })(withRouter(CardDescription))
