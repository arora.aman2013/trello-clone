import React, { Component } from 'react'

export class CheckListItems extends Component {
    state = {
        showButton: false
    }
    renderCheckbox = () => {
        if(this.props.checklistItem.name){
            if(this.props.checklistItem.state === 'complete')
            return(
                <input type = 'checkbox' style = {{verticalAlign: 'middle'}} checked = 'true' onClick = {this.onChange}/>
            )
            else
            return(
                <input type = 'checkbox' style = {{verticalAlign: 'middle'}} onClick = {this.onChange}/>
            )
        }
    }
    onChange = (e) => {
        e.stopPropagation();
        console.log('True')
    }
    strikeThrough = () => {
        if(this.props.checklistItem.state === 'complete')
            return{
                textDecoration: 'line-through'
            }
        else
            return{
                textDecoration: 'none'
            } 
    }
    button = (e) => {
        return(
            <button style = {{float: 'right', border: 'none', backgroundColor: 'transparent',verticalAlign: 'middle', cursor: 'pointer',}} >X</button>
        )
    }
    render() {
        return (
            <p style = {this.strikeThrough()} onMouseEnter = {()=> {this.setState({showButton: true})}} onMouseLeave = {()=> {this.setState({showButton: false})}}>
                {this.renderCheckbox()}
                {this.props.checklistItem.name}
                {this.state.showButton? this.button(): null}
            </p>
        )
    }
}

export default CheckListItems
