import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import { deleteCard } from '../actions/trelloActions'
import { connect } from 'react-redux'
class Cards extends Component {
    state = {
        showButtons: false
    }
    onClick = (e) => {
        // console.log(this.props.card.id)
        this.props.history.push(`/card/${this.props.card.id}`)
    }
    cardDelete = async(e) => {
        console.log(this.props.card.id)
        await this.props.deleteCard(this.props.card.id)
        this.props.history.push(`/`)
    }
    cardEdit = (e) => {
        e.stopPropagation()
        this.props.history.push(`/edit/${this.props.card.id}/${this.props.card.name}`)
    }
    button = (e) => {
        return(
            <div>
                <button style = {{backgroundColor: 'transparent', border: 'none', cursor: 'pointer', float: 'right'}} className = {'button'} onClick = {this.cardEdit}>e</button>
                <button style = {{backgroundColor: 'transparent', border: 'none', cursor: 'pointer'}} className = {'button'} onClick = {this.cardDelete}>x</button>
            </div>
        )
    }
    render() {
        return (
            <div style = {cardStyle} onMouseEnter = {() => {this.setState({showButtons: true})}} onMouseLeave = {() => {this.setState({showButtons: false})}} onClick = {this.onClick}>
            <div >
                {this.props.card.name}
            </div>
                {this.state.showButtons? this.button(): null}
            </div>
        )
    }
}

const cardStyle = {
    cursor: 'pointer',
    backgroundColor: 'white',
    padding: '4% 4% 4% 1%',
    margin: '2%',
    marginTop: '3%',
    borderRadius: '5px',
    display: 'flex',
    justifyContent: 'space-between'
}

export default connect(null, { deleteCard })(withRouter(Cards))
