export const getListsAndCards = () => (
    {
    type: 'getAll'
    }
)

export const getCard = (data) => (
    {
        type: 'getCards',
        data
    }
)

export const getCheckList = (data) => (
    {
        type: 'getCheck',
        data
    }
)

export const addCard = (listId, input) => (
    // console.log(listId + input),
    {
        type: 'Card',
        listId,
        input
    }
)

export const addList = (input) => (
    // console.log(input),
    {
        type: 'list',
        input
    }
)

export const deleteCard = (cardId) => (
    // console.log(input),
    {
        type: 'delCard',
        cardId
    }
)

export const editCard = (cardId, input) => (
    // console.log(input),
    {
        type: 'edCard',
        cardId,
        input
    }
)

export const addCheckItem = (checklistId, input) => (
    // console.log(checklistId, input),
    {
        type: 'addItem',
        checklistId,
        input
    }
)

export const addChecklist = (cardId, input) => (
    // console.log(cardId, input),
    {
        type: 'Checklist',
        cardId,
        input
    }
)

export const deleteChecklist = (checklistId) => (
    console.log(checklistId),
    {
        type: 'delete',
        checklistId
    }
)